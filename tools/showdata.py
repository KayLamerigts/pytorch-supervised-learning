import click
from torchvision.transforms import ToPILImage

from dataset.common import get_datasets


@click.command()
@click.argument('directory')
@click.option('-n', default=5)
def show_data(directory, n):
    train_set, test_set = get_datasets(normalize=False)

    for i, ((train_sample, _), (test_sample, _)) in enumerate(zip(train_set,
                                                                  test_set)):
        train_sample = ToPILImage()(train_sample)
        train_sample.save(f'{directory}/train-{i}.png')
        test_sample = ToPILImage()(test_sample)
        test_sample.save(f'{directory}/test-{i}.png')
        if i == n:
            break
