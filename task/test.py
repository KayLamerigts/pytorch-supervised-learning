import click
import torch

from core.dataloader import create_data_loaders
from core.device import get_device, print_system_info
from core.loss import create_loss
from core.metric import calculate_metrics
from core.state import load_weights, save_metrics
from network.common import create_network


@click.command()
@click.option('-n', default=None, type=int)
def test(n):
    print_system_info()
    device = get_device()

    _, _, test_loader = create_data_loaders()

    network = create_network()
    network.to(device)
    load_weights(network)
    network.eval()

    logits = []
    targets = []
    with torch.no_grad():
        for i, (batch_images, batch_targets) in enumerate(test_loader):
            batch_images_device = batch_images.to(device)
            batch_logits_device = network.forward(batch_images_device)
            batch_logits = batch_logits_device.cpu()

            logits.append(batch_logits)
            targets.append(batch_targets)

            if n and i >= n:
                break

    logits = torch.cat(logits, 0)
    targets = torch.cat(targets, 0)

    loss = create_loss()
    overall_loss = loss(logits, targets).item()
    overall_metrics = calculate_metrics(logits, targets)
    overall_metrics['loss'] = overall_loss

    save_metrics(overall_metrics)

    print('Testing done:', overall_metrics)
