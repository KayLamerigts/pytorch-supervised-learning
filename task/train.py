import torch

import click

from core.dataloader import create_data_loaders
from core.device import get_device, set_seed, print_system_info
from core.logging import log_train_progress, create_tensorboard_writer
from core.loss import create_loss
from core.metric import calculate_metrics
from core.network import initialize_weights
from core.optimizer import create_optimizer
from core.settings import settings
from core.state import has_stored_state, load_state, save_state
from network.common import create_network


@click.command()
def train():
    print_system_info()
    set_seed()
    device = get_device()

    train_loader, dev_loader, _ = create_data_loaders()
    loss = create_loss()

    tb_writer = create_tensorboard_writer()

    # first create network and move it to the desired device
    network = create_network()
    network.to(device)
    # after moving to device, create the optimizer
    optimizer, scheduler = create_optimizer(network)

    if has_stored_state():
        step = load_state(network, optimizer, scheduler)
    else:
        initialize_weights(network)
        step = 0

    num_steps = settings['train']['steps']
    num_steps_interval = settings['train']['steps-interval']

    while step < num_steps:
        for _ in range(num_steps_interval):
            train_loss, train_metrics = train_step(
                network, optimizer, scheduler, train_loader, loss, device)
        dev_loss, dev_metrics = dev_step(network, dev_loader, loss, device)

        step += num_steps_interval
        save_state(network, optimizer, scheduler, step)
        log_train_progress(step, tb_writer, network, train_loss, dev_loss,
                           train_metrics, dev_metrics)

    print('Training done.')


def train_step(network, optimizer, scheduler, data_loader, loss, device):
    # set to train mode
    network.train()

    batch_images, batch_targets = next(data_loader)

    # clear gradient buffer
    optimizer.zero_grad()

    # forward
    images = batch_images.to(device)
    targets = batch_targets.to(device)
    logits = network.forward(images)

    # weight update
    batch_loss = loss(logits, targets)
    batch_loss.backward()
    optimizer.step()
    scheduler.step()

    return batch_loss.data.item(), calculate_metrics(logits, targets)


def dev_step(network, data_loader, loss, device):
    # set to test mode
    network.eval()

    batch_images, batch_targets = next(data_loader)

    # forward
    with torch.no_grad():
        images = batch_images.to(device)
        targets = batch_targets.to(device)
        logits = network.forward(images)

    return loss(logits, targets), calculate_metrics(logits, targets)
