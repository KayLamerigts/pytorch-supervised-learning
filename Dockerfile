FROM pytorch/pytorch:1.0-cuda10.0-cudnn7-runtime

ENV PC_SETTINGS /app/configurations/cifar10-resnet18.yaml
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

RUN pip install --upgrade pip
RUN pip install click tensorboardX

COPY . /app
CMD python /app/cli.py train
