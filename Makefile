

job:
    paperspace jobs create --container pytorch/pytorch:0.4.1-cuda9-cudnn7-runtime --machineType GPU+ --command './run.sh' --project 'Test PC'

tb-local:
    sudo docker run -it --mount type=bind,source=/home/kay/volumes/pytorch-classification/logs,target=/tb -p 6006:6006 tensorflow/tensorflow sh -c 'tensorboard --logdir=/tb'