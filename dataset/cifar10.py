from torchvision.datasets import CIFAR10

from augmentation.common import get_transforms
from core.settings import settings

NORMALIZE_MEAN = (0.4914, 0.4822, 0.4465)
NORMALIZE_STD = (0.2023, 0.1994, 0.2010)


def get_cifar10_datasets(normalize):
    transform_train, transform_test = get_transforms(
        normalize, NORMALIZE_MEAN, NORMALIZE_STD
    )

    cache_path = settings['data']['download-path']
    train_set = CIFAR10(root=cache_path, train=True, download=True,
                        transform=transform_train)
    test_set = CIFAR10(root=cache_path, train=False, download=True,
                       transform=transform_test)

    return train_set, test_set







