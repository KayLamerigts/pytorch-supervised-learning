from core.settings import settings
from dataset.cifar10 import get_cifar10_datasets


def get_datasets(normalize=True):
    name = settings['data']['name']

    if name == 'cifar10':
        train, test = get_cifar10_datasets(normalize)
    else:
        raise ValueError(f'Dataset {name} does not exist.')

    print(f'Loaded {name} ({len(train)} train samples, {len(test)} '
          f'test samples).')

    return train, test
