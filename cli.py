import click

from task.test import test
from task.train import train
from tools.showdata import show_data


@click.group()
def cli():
    # This is only an entry point
    pass


cli.add_command(test)
cli.add_command(train)
cli.add_command(show_data)


if __name__ == "__main__":
    cli()
