import pytest
import torch

from core.metric import accuracy


def cast_to_tensors(logits, targets):
    return torch.FloatTensor(logits), torch.LongTensor(targets)


@pytest.mark.parametrize("logits, targets, expected", [
    ([[0.6, 0.2, 0.2], [0.3, 0.3, 0.4]], [0, 1], 0.5),
    ([[0.0, 1]], [0], 0),
    ([[0.4, 0.6], [0.4, 0.6], [0.3, 0.7]], [1, 1, 1], 1)

])
def test_accuracy(logits, targets, expected):
    logits, targets = cast_to_tensors(logits, targets)
    assert accuracy(logits, targets) == expected
