import os
from pathlib import Path

import yaml

ENV_SETTING_PATH_NAME = 'PC_SETTINGS'

settings = None

if ENV_SETTING_PATH_NAME in os.environ:
    settings_file_path = Path(os.environ[ENV_SETTING_PATH_NAME])

    print(f'Using setting file {settings_file_path}')

    if not settings_file_path.exists():
        raise EnvironmentError(f'{settings_file_path.as_posix()} does not'
                               f'exists.')

    with open(settings_file_path.as_posix(), 'r') as f:
        settings = yaml.load(f)
else:
    settings = None
