import torch

from core.settings import settings


def create_optimizer(network):
    config = settings['train']['optimizer']

    optimizer = _create_optimizer(network, config)
    scheduler = _create_scheduler(optimizer, config) \
        if 'scheduler' in config else None

    return optimizer, scheduler


def _create_optimizer(network, config):
    name = config['name']

    if name == 'sgd':
        learning_rate = config.get('learning-rate', 0.1)
        momentum = config.get('momentum', 0)
        dampening = config.get('dampening', 0)
        weight_decay = config.get('decay', 0)
        nesterov = config.get('nesterov', False)

        return torch.optim.SGD(network.parameters(), learning_rate, momentum,
                               dampening, weight_decay, nesterov)

    if name == 'adam':
        learning_rate = config.get('learning-rate', 1e-3)
        beta_1 = config.get('beta-1', 0.9)
        beta_2 = config.get('beta-2', 0.999)
        epsilon = config.get('epsilon', 1e-8)
        decay = config.get('decay', 0)
        amsgrad = config.get('amsgrad', False)

        return torch.optim.Adam(network.parameters(), learning_rate,
                                (beta_1, beta_2), epsilon, decay, amsgrad)

    raise ValueError(f'Algorithm ({name}) is unknown.')


def _create_scheduler(optimizer, config):
    config = config['scheduler']
    name = config['name']

    if name == 'multistep':
        milestones = config['milestones']
        gamma = config.get('gamma', 0.1)

        return torch.optim.lr_scheduler.MultiStepLR(optimizer,
                                                    milestones,
                                                    gamma)

    raise ValueError(f'Scheduler ({name}) is unknown.')
