import sys

import numpy as np
import torch

from core.settings import settings


def get_device():
    if torch.cuda.is_available():
        return torch.device("cuda:0")
    else:
        return torch.device("cpu")


def set_seed():
    seed = get_seed()
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed)


def get_seed():
    return settings.get('seed', 4520)


def print_system_info():
    cuda_available = torch.cuda.is_available()

    print("System information:")
    print('  Python: ', sys.version)
    print("  OS:     ", sys.platform)
    print("  PyTorch:", torch.__version__)
    print("  Numpy:  ", np.__version__)
    print("  CUDA:   ", cuda_available)

    if cuda_available:
        print('  cudnn:  ', torch.backends.cudnn.version())
        print('  count: ', torch.cuda.device_count())
