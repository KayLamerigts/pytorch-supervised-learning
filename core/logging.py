from pathlib import Path

from tensorboardX import SummaryWriter

from core.settings import settings


def log_train_progress(step, writer, network, train_loss, dev_loss,
                       train_metrics, dev_metrics):
    print(f'{step} | train: {train_loss:.3f}, dev: {dev_loss:.3f}')
    writer.add_scalars('loss', {'train': train_loss, 'dev': dev_loss}, step)

    for metric in train_metrics:
        writer.add_scalars(metric, {
            'train': train_metrics[metric],
            'dev': dev_metrics[metric]
        }, step)

    if settings['train']['log-parameters']:
        log_parameters(step, writer, network)


def create_tensorboard_writer():
    log_path = Path(settings['train']['log-path'])
    return SummaryWriter(log_dir=log_path.absolute().as_posix())


def log_parameters(step, writer, network):
    param_prefix = 'param/'
    grad_prefix = 'grad/'

    for name, param in network.named_parameters():
        name = name.replace('.', '/')
        writer.add_histogram(param_prefix + name, param.detach().cpu().numpy(),
                             step)
        writer.add_histogram(grad_prefix + name, param.grad.cpu().numpy(), step)
