import torch

from core.settings import settings


def create_loss():
    config = settings['train']['loss']
    name = config['name']

    if name == 'cross-entropy':
        weight = config.get('weight', None)
        size_average = config.get('size-average', None)
        ignore_index = config.get('ignore-index', 100)
        reduce = config.get('reduce', None)
        reduction = config.get('reduction', 'elementwise_mean')

        return torch.nn.CrossEntropyLoss(weight, size_average, ignore_index,
                                         reduce, reduction)

    raise ValueError(f'Loss "{name}" is unknown.')
