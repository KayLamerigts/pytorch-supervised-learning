import sys

from core.settings import settings


def calculate_metrics(logits, targets):
    metrics = dict()
    module = sys.modules[__name__]
    metrics_to_collect = settings['train']['metrics']

    for metric_name in metrics_to_collect:
        metric_function = getattr(module, metric_name)
        metrics[metric_name] = metric_function(logits, targets)

    return metrics


def accuracy(logits, targets):
    predicted = logits.argmax(1)
    size = logits.size(0)
    return predicted.eq(targets).sum().item() / size


def brier(logits, targets):
    pass


def precision(logits, targets):
    pass


def recall(logits, targets):
    pass


def f1(logits, targets):
    pass


def auc(logits, targets):
    pass

