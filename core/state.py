import pickle
from pathlib import Path

import torch

from core.settings import settings


def has_stored_state():
    checkpoint_file = Path(settings['train']['checkpoint-path'])
    return checkpoint_file.exists()


def save_state(network, optimizer, scheduler, step):

    checkpoint_file = Path(settings['train']['checkpoint-path'])

    state = {
        'network': network.state_dict(),
        'optimizer': optimizer.state_dict(),
        'scheduler': scheduler.state_dict(),
        'step': step,
    }

    torch.save(state, checkpoint_file.as_posix())


def load_state(network, optimizer, scheduler):
    checkpoint_file = Path(settings['train']['checkpoint-path'])

    # load checkpoint
    checkpoint = torch.load(checkpoint_file.as_posix())

    # set states
    network.load_state_dict(checkpoint['network'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler.load_state_dict(checkpoint['scheduler'])

    # set counters
    step = checkpoint['step']

    return step


def load_weights(network):
    checkpoint_file = Path(settings['train']['checkpoint-path'])
    checkpoint = torch.load(checkpoint_file.as_posix())

    network.load_state_dict(checkpoint['network'])


def save_metrics(metrics):
    path = settings['test']['metrics-path']
    with open(path, 'wb') as f:
        pickle.dump(metrics, f)
