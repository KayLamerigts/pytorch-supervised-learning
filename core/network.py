import numpy as np
from torch import nn

from core.settings import settings


def initialize_weights(network):
    name = settings['network']['initialization-method']

    initialization_functions = {
        'kaiming': lambda w: nn.init.kaiming_normal_(w, mode='fan_out',
                                                     nonlinearity='relu'),
        'xavier': nn.init.xavier_normal_}
    initialization_function = initialization_functions[name]

    for module in network.modules():
        if isinstance(module, nn.Conv2d):
            initialization_function(module.weight)
        elif isinstance(module, nn.BatchNorm2d):
            nn.init.constant_(module.weight, 1)
            nn.init.constant_(module.bias, 0)


def get_number_of_parameters(network):
    trainable_parameters = [p for p in network.parameters() if p.requires_grad]
    return sum([np.prod(p.size()) for p in trainable_parameters])
