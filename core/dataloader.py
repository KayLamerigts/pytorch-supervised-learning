from math import floor, ceil

import torch
from torch.utils.data import Dataset, random_split

from core.settings import settings
from dataset.common import get_datasets


def create_data_loaders():
    train_set, test_set = get_datasets()
    train_length = floor(0.9 * len(train_set))
    dev_length = ceil(0.1 * len(train_set))
    train_set, dev_set = random_split(train_set, [train_length, dev_length])

    batch_size = settings['train']['batch-size']

    train_loader = torch.utils.data.DataLoader(
        train_set,
        batch_size=batch_size,
        shuffle=True,
        num_workers=2
    )

    dev_loader = torch.utils.data.DataLoader(
        dev_set,
        batch_size=batch_size,
        shuffle=True,
        num_workers=2
    )

    test_loader = torch.utils.data.DataLoader(
        test_set,
        batch_size=batch_size,
        shuffle=True,
        num_workers=2
    )

    train_loader_infinite = infinite_loader(train_loader)
    dev_loader_infinite = infinite_loader(dev_loader)

    return train_loader_infinite, dev_loader_infinite, test_loader


def infinite_loader(data_loader):
    data_loader_iter = iter(data_loader)
    while True:
        try:
            yield next(data_loader_iter)
        except StopIteration:
            data_loader_iter = iter(data_loader)
            yield next(data_loader_iter)
