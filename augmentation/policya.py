from torchvision.transforms import RandomVerticalFlip, RandomHorizontalFlip, \
    Pad, RandomRotation, RandomCrop, ColorJitter

from core.settings import settings


def get_transforms():
    config = settings['data']['augmentations']

    pad_size = config['pad']['size']
    pad_mode = config['pad'].get('mode', 'reflect')

    rotate_degrees = config.get('rotate', 360)

    crop_size = config['crop']

    vertical_p = config.get('vertical-flip', 0.5)
    horizontal_p = config.get('horizontal-flip', 0.5)

    color_jitter_brightness = config['color-jitter'].get('brightness', 0.5)
    color_jitter_contrast = config['color-jitter'].get('contrast', 0.5)
    color_jitter_saturation = config['color-jitter'].get('saturation', 0.5)
    color_jitter_hue = config['color-jitter'].get('hue', 0.1)

    transforms = [
        Pad(pad_size, 0, pad_mode),
        RandomRotation(rotate_degrees),
        RandomCrop(crop_size),
        RandomVerticalFlip(vertical_p),
        RandomHorizontalFlip(horizontal_p),
        ColorJitter(color_jitter_brightness, color_jitter_contrast,
                    color_jitter_saturation, color_jitter_hue),
    ]

    return transforms
