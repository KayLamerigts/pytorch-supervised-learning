from torchvision.transforms import Normalize, ToTensor, Compose

import augmentation.policya as policy_a

from core.settings import settings


def get_augmentation_policy():
    config = settings['data']['augmentation-policy']
    name = config['name']

    if name == 'a':
        return policy_a.get_transforms()

    raise ValueError(f"Data augmentation policy '{name}' is unknown.")


def get_transforms(normalize=True, normalize_mean=None, normalize_std=None):
    transform_train = get_augmentation_policy()
    transform_test = []

    if normalize:
        if normalize_mean is None or normalize_std is None:
            raise ValueError('Mean and std required for normalization.')

        normalize_extension = [
            Normalize(normalize_mean, normalize_std),
            ToTensor()
        ]

        transform_train.extend(normalize_extension)
        transform_test.extend(normalize_extension)

    transform_train = Compose(transform_train)
    transform_test = Compose(transform_test)

    return transform_train, transform_test
