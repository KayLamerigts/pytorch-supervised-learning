#!/usr/bin/env bash

export PC_SETTINGS=configurations/cifar10-resnet18.yaml
export LC_ALL=C.UTF-8
export LANG=C.UTF-8

pip install --upgrade pip
pip install click tensorboardX

python cli.py train
python cli.py test
