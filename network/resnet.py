import torch.nn as nn

from core.settings import settings
from network.block.common import get_block_constructor
from network.block.head import Head
from network.block.tail import Tail


class ResNet(nn.Module):
    def __init__(self):
        super(ResNet, self).__init__()

        channels = settings['network']['channels']
        blocks = settings['network']['blocks']
        block_constructor = get_block_constructor()

        self.head = Head()
        self.body = _make_stages(blocks, channels, block_constructor)
        self.tail = Tail()

    def forward(self, x):
        x = self.head(x)
        x = self.body(x)
        x = self.tail(x)
        return x


def _make_stages(blocks, channels, block_constructor):
    stages = []
    for stage_ix in range(len(blocks)):
        stage_filters = channels * 2 ** stage_ix
        stages.append(_make_blocks(stage_filters, blocks[stage_ix],
                                   block_constructor,
                                   down_sample=stage_ix > 0))

    return nn.Sequential(*stages)


def _make_blocks(channels, size, block_constructor, down_sample):
    channels_first = channels // 2 if down_sample else channels
    blocks = [block_constructor(channels_first, channels, down_sample)]
    for i in range(size - 1):
        blocks.append(block_constructor(channels, channels, False))

    return nn.Sequential(*blocks)
