from core.network import get_number_of_parameters
from core.settings import settings
from network.resnet import ResNet


def create_network():
    type = settings['network']['type']

    if type == 'resnet':
        network = ResNet()
    else:
        network = ResNet()

    print(f'Loaded {type} ({ get_number_of_parameters(network):,} parameters)')

    return network
