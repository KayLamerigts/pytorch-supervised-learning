import torch.nn as nn

from core.settings import settings


class Tail(nn.Module):
    def __init__(self):
        super(Tail, self).__init__()

        channels_in = settings['network']['tail']['channels-in']
        channels_out = settings['network']['tail']['channels-out']

        self.average_pool = nn.AdaptiveAvgPool2d(1)
        self.linear = nn.Linear(channels_in, channels_out)

    def forward(self, x):
        x = self.average_pool(x)
        x = x.view(x.size(0), -1)
        x = self.linear(x)
        return x
