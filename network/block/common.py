from core.settings import settings
from network.block.residual import ResidualBlock


def get_block_constructor():
    name = settings['network']['block-type']

    if name == 'residual-block':
        return ResidualBlock

    return ResidualBlock
