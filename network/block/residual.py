import torch.nn as nn


class ResidualBlock(nn.Module):
    def __init__(self, in_channels, channels, down_sample=False):
        super(ResidualBlock, self).__init__()

        self.down_sample = down_sample

        self.conv_down, self.bn_down, stride_first = None, None, 1
        if self.down_sample:
            self.bn_down = nn.BatchNorm2d(in_channels)
            self.conv_down = nn.Conv2d(in_channels, channels,
                                       kernel_size=1, stride=2)
            stride_first = 2

        self.bn1 = nn.BatchNorm2d(in_channels)
        self.relu1 = nn.ReLU(inplace=True)
        self.conv1 = nn.Conv2d(in_channels, channels, 3, padding=1,
                               stride=stride_first)

        self.bn2 = nn.BatchNorm2d(channels)
        self.relu2 = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(channels, channels, 3, padding=1)

    def forward(self, x):
        residual = x

        y = self.bn1(x)
        y = self.relu1(y)
        y = self.conv1(y)

        y = self.bn2(y)
        y = self.relu2(y)
        y = self.conv2(y)

        if self.down_sample:
            residual = self.bn_down(residual)
            residual = self.conv_down(residual)

        return y + residual
