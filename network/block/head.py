import torch.nn as nn

from core.settings import settings


class Head(nn.Module):
    def __init__(self):
        super(Head, self).__init__()

        config_head = settings['network']['head']
        head_layers = [get_head_layer(c) for c in config_head]
        self.head = nn.Sequential(*head_layers)

    def forward(self, x):
        return self.head(x)


def get_head_layer(config_layer):
    type = config_layer['type']

    if type == 'conv':
        channels_in = config_layer['channels-in']
        channels_out = config_layer['channels-out']
        kernel_size = config_layer['kernel']
        stride = config_layer['stride']
        padding = config_layer['padding']

        return nn.Conv2d(channels_in, channels_out, kernel_size,
                         stride, padding)

    elif type == 'batch-norm':
        channels = config_layer['channels']
        return nn.BatchNorm2d(channels)

    elif type == 'activation':
        return nn.ReLU(inplace=True)

    elif type == 'max-pool':
        kernel_size = config_layer['kernel']
        stride = config_layer['stride']
        padding = config_layer['padding']
        return nn.MaxPool2d(kernel_size, stride, padding)
