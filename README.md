# The PyTorch Supervised Playground

A playground where you can run experiments. Flexible enough not be constrained by the structure, powerful enough
to quickly gain results.

Note: after watching [Object-Oriented Programming is Bad](https://www.youtube.com/watch?v=QM1iUe6IofM) by Brian Will I 
was intrigued and wanted to build something with his philosophy in mind. So this project does not contain any class 
other than those imported from third-party packages.

# Structure

Three components:

- network
- dataset
- data augmentation policy


# Settings

Environment variable `PC_SETTINGS` defines the settings file. In settings you define the architecture, dataset,
hyperparameters, etc.

# Run

## Local

python cli.py train

## Paperspace mix

run.sh

## docker-compose

docker-compose up


https://arxiv.org/pdf/1812.01187v1.pdf
